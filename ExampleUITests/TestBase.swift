//
//  TestBase.swift
//  ExampleUITests
//
//  Created by Moataz Eldebsy on 15.04.20.
//  Copyright © 2020 Venmo. All rights reserved.
//

import Foundation
import XCTest
class TestBase: XCTestCase {
    let app = XCUIApplication()

    override func setUp() {
        super.setUp()
        app.launch()
    }
}
