import Foundation
import XCTest
class ExampleUITests: TestBase {
    func testWhichPasses() {
        XCTContext.runActivity(testing: 1) { _ in
            let query = app.staticTexts["exampleLabel"]
            XCTAssertTrue(query.exists, query.debugDescription)
        }
    }

    func testWhichFails() {
        XCTContext.runActivity(testing: 2) { _ in
            // let query = app.staticTexts["non-existant accessibility identifier"]
            // XCTAssertTrue(query.exists, query.debugDescription)
        }
    }

    func testWithFailingAssertions() {
        XCTContext.runActivity(testing: 3) { _ in
            let query = app.staticTexts["non-existant accessibility identifier"]
            XCTAssertTrue(query.exists, query.debugDescription)
        }
    }

    func testWithPassingAssertions() {
        XCTContext.runActivity(testing: 4) { _ in
            let query = app.staticTexts["exampleLabel"]
            XCTAssertTrue(query.exists, query.debugDescription)
        }
    }

    func test1() {
        XCTContext.runActivity(testing: 5) { _ in
            XCTAssertTrue(false)
        }
    }

    func test2() {
        XCTContext.runActivity(testing: 66) { _ in
            XCTAssertTrue(false)
        }
    }
}
