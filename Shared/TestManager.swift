import Foundation
import QuizTrain
import XCTest

/*
 Principal Class for test targets owned by their Bundle. This should be accessed
 using its singleton property: TestManager.sharedInstance

 Performs logic required before any tests run and after all tests complete.
 */
final class TestManager: NSObject {
    let voloManager: QuizTrainManager

    override init() {
        print("\n========== TestManager ==========\n")
        defer { print("\n====================================\n") }

        print("TestRail setup started.")
        let objectAPI = QuizTrain.ObjectAPI(username: CI.testRailUsername, secret: CI.testRailSecret, hostname: CI.testRailHostname, port: 443, scheme: "https")
        var quizTrainManager: QuizTrainManager!
        let group = DispatchGroup()
        group.enter()
        DispatchQueue.global().async {
            QuizTrainProject.populatedProject(forProjectId: 1, objectAPI: objectAPI) { outcome in
                switch outcome {
                case let .failure(error):
                    print("TestRail setup failed: \(error)")
                    fatalError(error.localizedDescription)
                case let .success(project):
                    quizTrainManager = QuizTrainManager(objectAPI: objectAPI, project: project)
                }
                group.leave()
            }
        }
        group.wait()
        voloManager = quizTrainManager
        XCTestObservationCenter.shared.addTestObserver(voloManager)
        print("TestRail setup completed.")

        super.init()

        TestManager._sharedInstance = self
    }

    deinit {
        XCTestObservationCenter.shared.removeTestObserver(self.voloManager)
    }

    // MARK: - Singleton

    private static var _sharedInstance: TestManager!

    static var sharedInstance: TestManager {
        return _sharedInstance
    }
}

// MARK: - Global

func logTest(_ caseIds: [Case.Id], withCaseId: Bool = true, withProjectName: Bool = false, withSuiteName: Bool = true, withSectionNames: Bool = true) {
    let caseTitles = TestManager.sharedInstance.voloManager.project.caseTitles(caseIds, withCaseId: withCaseId, withProjectName: withProjectName, withSuiteName: withSuiteName, withSectionNames: withSectionNames)
    for caseTitle in caseTitles {
        print(caseTitle)
    }
}

func logTest(_ caseIds: Case.Id..., withCaseId: Bool = true, withProjectName: Bool = false, withSuiteName: Bool = true, withSectionNames: Bool = true) {
    logTest(caseIds, withCaseId: withCaseId, withProjectName: withProjectName, withSuiteName: withSuiteName, withSectionNames: withSectionNames)
}

func logAndStartTesting(_ caseIds: [Case.Id]) {
    logTest(caseIds)
    startTesting(caseIds)
}

func logAndStartTesting(_ caseIds: Case.Id...) {
    logTest(caseIds)
    startTesting(caseIds)
}

func startTesting(_ caseIds: [Case.Id]) {
    TestManager.sharedInstance.voloManager.startTesting(caseIds)
}

func startTesting(_ caseIds: Case.Id...) {
    TestManager.sharedInstance.voloManager.startTesting(caseIds)
}

func completeTesting(_ caseIds: [Case.Id], withResultIfUntested result: QuizTrainManager.Result = .passed, comment: String? = nil) {
    TestManager.sharedInstance.voloManager.completeTesting(caseIds, withResultIfUntested: result, comment: comment)
}

func completeTesting(_ caseIds: Case.Id..., withResultIfUntested result: QuizTrainManager.Result = .passed, comment: String? = nil) {
    TestManager.sharedInstance.voloManager.completeTesting(caseIds, withResultIfUntested: result, comment: comment)
}
