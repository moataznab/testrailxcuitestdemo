//
//  Environment.swift
//  ExampleUITests
//
//  Created by Moataz Eldebsy on 15.04.20.
//  Copyright © 2020 Venmo. All rights reserved.
//

import Foundation
struct Environment {
    static var enableTestRail: String = Environment.variable(named: "ENABLE_TESTRAIL") ?? CI.enableTestRail
    static var testRailUsername: String = Environment.variable(named: "TESTRAIL_USERNAME") ?? CI.testRailUsername
    static var testRailSecret: String = Environment.variable(named: "TESTRAIL_SECRET") ?? CI.testRailSecret
    static var testRailHostname: String = Environment.variable(named: "TESTRAIL_HOSTNAME") ?? CI.testRailHostname
    static func variable(named name: String) -> String? {
        let processInfo = ProcessInfo.processInfo
        guard let value = processInfo.environment[name] else {
            return nil
        }
        return value
    }
}
