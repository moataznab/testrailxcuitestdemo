//
//  CI.swift
//  Example
//
//  Created by Moataz Eldebsy on 16.04.20.
//  Copyright © 2020 Venmo. All rights reserved.
//

import Foundation
struct CI {
    static var enableTestRail: String = "$(ENABLE_TESTRAIL)"
    static var testRailUsername: String = "$(TESTRAIL_USERNAME)"
    static var testRailSecret: String = "$(TESTRAIL_SECRET)"
    static var testRailHostname: String = "$(TESTRAIL_HOSTNAME)"
}
