@testable import QuizTrain
import XCTest

protocol ValidatableObjectProvider {
    var validObject: Validatable { get }
    var invalidObject: Validatable { get }
}
