extension ObjectAPI.ObjectConversionError: CustomDebugStringConvertible {
    public var debugDescription: String {
        var description = "ObjectAPI.ObjectConversionError"
        switch self {
        case .couldNotConvertObjectToData:
            description += ".couldNotConvertObjectToData:\n\n\(debugDetails)\n"
        case .couldNotConvertObjectsToData:
            description += ".couldNotConvertObjectsToData:\n\n\(debugDetails)\n"
        }
        return description
    }
}

extension ObjectAPI.ObjectConversionError: DebugDetails {
    public var debugDetails: String {
        let details: String

        switch self {
        case let .couldNotConvertObjectToData(object, json, error):
            details = """
            _____OBJECT_____

            \(object)

            _____JSON_____

            \(json)

            _____ERROR_____

            \(error)
            """
        case let .couldNotConvertObjectsToData(objects, json, error):
            details = """
            _____OBJECTS_____

            \(objects)

            _____JSON_____

            \(json)

            _____ERROR_____

            \(error)
            """
        }

        return details
    }
}
