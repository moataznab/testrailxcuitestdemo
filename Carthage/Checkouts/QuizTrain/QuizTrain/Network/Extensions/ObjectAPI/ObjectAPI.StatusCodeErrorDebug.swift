extension ObjectAPI.StatusCodeError: CustomDebugStringConvertible {
    public var debugDescription: String {
        var description = "ObjectAPI.StatusCodeError"
        switch self {
        case .clientError:
            description += ".clientError:\n\n\(debugDetails)\n"
        case .otherError:
            description += ".otherError:\n\n\(debugDetails)\n"
        case .serverError:
            description += ".serverError:\n\n\(debugDetails)\n"
        }
        return description
    }
}

extension ObjectAPI.StatusCodeError: DebugDetails {
    public var debugDetails: String {
        let details: String
        switch self {
        case let .clientError(clientError):
            details = clientError.debugDetails
        case let .otherError(requestResult):
            details = requestResult.debugDetails
        case let .serverError(serverError):
            details = serverError.debugDetails
        }
        return details
    }
}
