extension ObjectAPI.MatchError: CustomDebugStringConvertible {
    public var debugDescription: String {
        var description = "ObjectAPI.MatchError"
        switch self {
        case .matchError:
            description += ".matchError:\n\n\(debugDetails)\n"
        case .otherError:
            description += ".otherError:\n\n\(debugDetails)\n"
        }
        return description
    }
}

extension ObjectAPI.MatchError where MatchErrorType: CustomDebugStringConvertible, OtherErrorType: CustomDebugStringConvertible {
    public var debugDescription: String {
        var description = "ObjectAPI.MatchError"
        switch self {
        case let .matchError(error):
            description += ".matchError: \(error.debugDescription)\n"
        case let .otherError(error):
            description += ".otherError: \(error.debugDescription)\n"
        }
        return description
    }
}

extension ObjectAPI.MatchError: DebugDetails {
    public var debugDetails: String {
        let details: String
        switch self {
        case let .matchError(error):
            details = "\(error)"
        case let .otherError(error):
            details = "\(error)"
        }
        return details
    }
}

extension ObjectAPI.MatchError where MatchErrorType: DebugDetails, OtherErrorType: DebugDetails {
    public var debugDetails: String {
        let details: String
        switch self {
        case let .matchError(error):
            details = "\(error.debugDetails)"
        case let .otherError(error):
            details = "\(error.debugDetails)"
        }
        return details
    }
}
