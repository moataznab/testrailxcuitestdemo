extension ObjectAPI.DataProcessingError: CustomDebugStringConvertible {
    public var debugDescription: String {
        var description = "ObjectAPI.DataProcessingError"
        switch self {
        case .couldNotConvertDataToJSON:
            description += ".couldNotConvertDataToJSON:\n\n\(debugDetails)\n"
        case .couldNotDeserializeFromJSON:
            description += ".couldNotDeserializeFromJSON:\n\n\(debugDetails)\n"
        case .invalidJSONFormat:
            description += ".invalidJSONFormat:\n\n\(debugDetails)\n"
        }
        return description
    }
}

extension ObjectAPI.DataProcessingError: DebugDetails {
    public var debugDetails: String {
        let details: String

        switch self {
        case let .couldNotConvertDataToJSON(data, error):
            details = """
            _____DATA_____

            \(data)

            _____ERROR_____

            \(error)
            """
        case let .couldNotDeserializeFromJSON(objectType, json):
            details = """
            _____DETAILS_____

            Object Type: \(objectType)

            _____JSON_____

            \(json)
            """
        case let .invalidJSONFormat(json):
            details = """
            _____JSON_____

            \(json)
            """
        }

        return details
    }
}
