extension ObjectAPI.DataRequestError: CustomDebugStringConvertible {
    public var debugDescription: String {
        var description = "ObjectAPI.DataRequestError"
        switch self {
        case .apiError:
            description += ".apiError:\n\n\(debugDetails)\n"
        case .dataProcessingError:
            description += ".dataProcessingError:\n\n\(debugDetails)\n"
        case .statusCodeError:
            description += ".statusCodeError:\n\n\(debugDetails)\n"
        }
        return description
    }
}

extension ObjectAPI.DataRequestError: DebugDetails {
    public var debugDetails: String {
        let details: String
        switch self {
        case let .apiError(error):
            details = error.debugDetails
        case let .dataProcessingError(error):
            details = error.debugDetails
        case let .statusCodeError(error):
            details = error.debugDetails
        }
        return details
    }
}
