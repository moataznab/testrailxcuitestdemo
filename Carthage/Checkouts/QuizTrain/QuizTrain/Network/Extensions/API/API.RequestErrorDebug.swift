extension API.RequestError: CustomDebugStringConvertible {
    public var debugDescription: String {
        var description: String = "API.RequestError"

        switch self {
        case .error:
            description += ".error:\n\n\(debugDetails)\n"
        case .invalidResponse:
            description += ".invalidResponse:\n\n\(debugDetails)\n"
        case .nilResponse:
            description += ".nilResponse:\n\n\(debugDetails)\n"
        }

        return description
    }
}

extension API.RequestError: DebugDetails {
    public var debugDetails: String {
        let details: String

        switch self {
        case let .error(request, error):
            details = """
            _____ERROR_____

            \(error)

            _____REQUEST_____

            \(request.httpMethod ?? "") \(request.url?.absoluteString ?? "")

            \(request.httpHeaderFieldsAsMultiLineString(omittingHeaders: ["AUTHORIZATION"]) ?? "")

            \(request.httpBodyAsUTF8 ?? "")
            """
        case let .invalidResponse(request, response):
            details = """
            _____REQUEST_____

            \(request.httpMethod ?? "") \(request.url?.absoluteString ?? "")

            \(request.httpHeaderFieldsAsMultiLineString(omittingHeaders: ["AUTHORIZATION"]) ?? "")

            \(request.httpBodyAsUTF8 ?? "")

            _____RESPONSE_____

            \(response)
            """
        case let .nilResponse(request):
            details = """
            _____REQUEST_____

            \(request.httpMethod ?? "") \(request.url?.absoluteString ?? "")

            \(request.httpHeaderFieldsAsMultiLineString(omittingHeaders: ["AUTHORIZATION"]) ?? "")

            \(request.httpBodyAsUTF8 ?? "")
            """
        }

        return details
    }
}
